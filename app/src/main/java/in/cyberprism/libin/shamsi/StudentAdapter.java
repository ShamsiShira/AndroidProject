package in.cyberprism.libin.shamsi;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by libin on 06/10/17.
 */

public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.StudentViewHolder> {

    ArrayList<StudentModel> models;

    public StudentAdapter(ArrayList<StudentModel> models) {
        this.models = models;
    }

    @Override
    public StudentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.recycler_item, parent, false);

        return new StudentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StudentViewHolder holder, int position) {
        StudentModel model = models.get(position);
        holder.textViewName.setText(model.getName());
        holder.textViewEmail.setText(model.getEmail());
        holder.textViewDept.setText(model.getDepartment());
    }

    @Override
    public int getItemCount() {
        return models.size();
    }

    class StudentViewHolder extends RecyclerView.ViewHolder {

        TextView textViewName;
        TextView textViewEmail;
        TextView textViewDept;

        public StudentViewHolder(View itemView) {
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewEmail = itemView.findViewById(R.id.textViewEmail);
            textViewDept = itemView.findViewById(R.id.textViewDept);
        }
    }
}
