package in.cyberprism.libin.shamsi;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        ArrayList<StudentModel> models = getData();
        StudentAdapter adapter = new StudentAdapter(models);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);

    }

    private ArrayList<StudentModel> getData() {
        ArrayList<StudentModel> models = new ArrayList<>();

        for (int i = 0; i < 20; i++) {
            StudentModel model = new StudentModel();
            model.setName("Student " + (i + 1));
            model.setEmail("student" + (i + 1) + "@gmail.com");
            model.setDepartment("CSE");

            models.add(model);
        }

        return models;
    }
}
